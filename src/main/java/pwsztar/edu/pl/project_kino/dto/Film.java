package pwsztar.edu.pl.project_kino.dto;

import javax.persistence.*;

@Entity
@Table(name ="film",schema = "kino")
public class Film {

    @Id
    @Column(name = "idf")
    private Integer idf;

    @Column(name = "tytul")
    private String tytul;

    @Column(name = "gatunek")
    private String gatunek;

    @Column(name = "produkcja")
    private String produkcja;

    @Column(name = "rok_premiery")
    private Integer rok_premiery;

    @Column(name = "rezyser")
    private String rezyser;

    @Column(name = "czas_trwania")
    private Integer czas_trwania;

    @Column(name = "wiek")
    private Integer wiek;

    @Column(name = "opis")
    private String opis;

    @Column(name = "zdjecie")
    private String zdjecie;




    public Film(int idf, String tytul, String gatunek, String produkcja, int rok_premiery, String rezyser, int czas_trwania, int wiek,String opis,String zdjecie) {
        this.idf = idf;
        this.tytul = tytul;
        this.gatunek = gatunek;
        this.produkcja = produkcja;
        this.rok_premiery = rok_premiery;
        this.rezyser = rezyser;
        this.czas_trwania = czas_trwania;
        this.wiek = wiek;
        this.opis = opis;
        this.zdjecie = zdjecie;
    }

    public Film() {

    }

    public int getIdf() {
        return idf;
    }

    public String getTytul() {
        return tytul;
    }

    public String getGatunek() {
        return gatunek;
    }

    public String getProdukcja() {
        return produkcja;
    }

    public int getRok_premiery() {
        return rok_premiery;
    }

    public String getRezyser() {
        return rezyser;
    }

    public int getCzas_trwania() {
        return czas_trwania;
    }

    public int getWiek() {
        return wiek;
    }

    public String getOpis() {
        return opis;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public void setIdf(Integer idf) {
        this.idf = idf;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public void setGatunek(String gatunek) {
        this.gatunek = gatunek;
    }

    public void setProdukcja(String produkcja) {
        this.produkcja = produkcja;
    }

    public void setRok_premiery(Integer rok_premiery) {
        this.rok_premiery = rok_premiery;
    }

    public void setRezyser(String rezyser) {
        this.rezyser = rezyser;
    }

    public void setCzas_trwania(Integer czas_trwania) {
        this.czas_trwania = czas_trwania;
    }

    public void setWiek(Integer wiek) {
        this.wiek = wiek;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    @Override
    public String toString() {
        return "Film{" +
                "idf=" + idf +
                ", tutyl='" + tytul + '\'' +
                ", gatunek='" + gatunek + '\'' +
                ", produkcja='" + produkcja + '\'' +
                ", rok_premiery=" + rok_premiery +
                ", rezyser='" + rezyser + '\'' +
                ", czas_trwania=" + czas_trwania +
                ", wiek=" + wiek +
                ", opis='" + opis + '\'' +
                ", zdjecie='" + zdjecie + '\'' +
                '}';
    }
}
