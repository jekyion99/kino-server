package pwsztar.edu.pl.project_kino.dto;

import lombok.Data;

@Data
public class Zam {
    private String typ;
    private Cena cena;
    private SeansPDF seans;
}