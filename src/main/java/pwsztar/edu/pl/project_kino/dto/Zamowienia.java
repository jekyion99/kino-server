package pwsztar.edu.pl.project_kino.dto;

import javax.persistence.*;

@Entity
@Table(schema = "kino",name = "zamowienia")
public class Zamowienia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_z;
    private int id_se;
    private String imie_kli;
    private String nazwisko_kli;
    private String email_kli;
    private String status;


    public Zamowienia(int id_se, String imie_kli, String nazwisko_kli, String email_kli, String status) {
        this.id_se = id_se;
        this.imie_kli = imie_kli;
        this.nazwisko_kli = nazwisko_kli;
        this.email_kli = email_kli;
        this.status = status;
    }

    public Zamowienia(){}

    public int getId_z() {
        return id_z;
    }

    public int getId_se() {
        return id_se;
    }

    public String getImie_kli() {
        return imie_kli;
    }

    public String getNazwisko_kli() {
        return nazwisko_kli;
    }

    public String getEmail_kli() {
        return email_kli;
    }

    public String getStatus() {
        return status;
    }


    public void setId_z(int id_z) {
        this.id_z = id_z;
    }

    public void setId_se(int id_se) {
        this.id_se = id_se;
    }

    public void setImie_kli(String imie_kli) {
        this.imie_kli = imie_kli;
    }

    public void setNazwisko_kli(String nazwisko_kli) {
        this.nazwisko_kli = nazwisko_kli;
    }

    public void setEmail_kli(String email_kli) {
        this.email_kli = email_kli;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Zamowienia{" +
                "id_z=" + id_z +
                ", id_se=" + id_se +
                ", imie_kli='" + imie_kli + '\'' +
                ", nazwisko_kli='" + nazwisko_kli + '\'' +
                ", email_kli='" + email_kli + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
