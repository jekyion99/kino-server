package pwsztar.edu.pl.project_kino.dto;

import javax.persistence.*;

@Entity
@Table(name = "newsletter", schema = "kino")
public class Newsletter {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "news_id")
    private int news_id;
    private String email;
    private Boolean zgoda;




    public String getEmail() {
        return email;
    }

    public int getNews_id() {
        return news_id;
    }

    public Boolean getZgoda() {
        return zgoda;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setZgoda(Boolean zgoda) {
        this.zgoda = zgoda;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }

    public Newsletter(String email, Boolean zgoda) {
        this.news_id = news_id;
        this.email = email;
        this.zgoda = zgoda;
    }

    public Newsletter(){}

    @Override
    public String toString() {
        return "Newsletter{" +
                "news_id=" + news_id +
                ", email='" + email + '\'' +
                ", zgoda=" + zgoda +
                '}';
    }
}
