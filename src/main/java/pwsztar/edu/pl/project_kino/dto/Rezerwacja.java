package pwsztar.edu.pl.project_kino.dto;


import lombok.Data;

@Data
public class Rezerwacja{

    private String imie;
    private String nazwisko;
    String nazwa_filmu;
    String data;
    String godz;
    Float cena;
    Integer rzad;
    Integer miejsce;
    String typ;
    String email;

    public Rezerwacja(String imie, String nazwisko, String nazwa_filmu, String data, String godz, Float cena, Integer rzad, Integer miejsce, String typ,String email) {
    this.imie=imie;
    this.nazwisko=nazwisko;
    this.nazwa_filmu=nazwa_filmu;
    this.data=data;
    this.godz=godz;
    this.cena=cena;
    this.rzad=rzad;
    this.miejsce=miejsce;
    this.typ=typ;
    this.email=email;
    }


}