package pwsztar.edu.pl.project_kino.dto;

import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import javax.persistence.*;
import javax.swing.text.StyledEditorKit;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import com.vladmihalcea.hibernate.type.array.BooleanArrayType;
import org.hibernate.annotations.TypeDefs;

@TypeDefs({
        @TypeDef(
                name = "boolean-array",
                typeClass = BooleanArrayType.class
        )
})
@Entity
@Table(name = "seans",schema = "kino")
public class Seans{


    @Id
    private int id_se;
    private int id_sali;
    private int idf;
    private LocalDate dzien;
    private int cena_zwykly;
    private int cena_vip;
    @Type(type = "boolean-array")
    @Column(name = "miejsca_zwykle",columnDefinition = "boolean[]" )
    private Boolean[] miejsca_zwykle;

    @Type(type = "boolean-array")
    @Column(name = "miejsca_vip",columnDefinition = "boolean[]")
    private Boolean[] miejsca_vip;


    private Time godzina;

    @Column (name ="zdjecie")
    private String zdjecie;

    @Column (name = "tytul")
    private String tytul;





    public int getCena_zwykly() {
        return cena_zwykly;
    }

    public int getCena_vip() {
        return cena_vip;
    }

    public int getId_se() {
        return id_se;
    }

    public int getId_sali() {
        return id_sali;
    }

    public int getIdf() {
        return idf;
    }

    public LocalDate getDzien() {
        return dzien;
    }

    public Time getGodzina() {
        return godzina;
    }

    public String getTytul() {
        return tytul;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public Boolean[] getMiejsca_zwykle() {
        return miejsca_zwykle;
    }

    public Boolean[] getMiejsca_vip() {
        return miejsca_vip;
    }

    public void setId_se(int id_se) {
        this.id_se = id_se;
    }

    public void setId_sali(int id_sali) {
        this.id_sali = id_sali;
    }

    public void setIdf(int idf) {
        this.idf = idf;
    }

    public void setDzien(LocalDate dzien) {
        this.dzien = dzien;
    }

    public void setGodzina(Time godzina) {
        this.godzina = godzina;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    public void setMiejsca_zwykle(Boolean[] miejsca_zwykle) {
        this.miejsca_zwykle = miejsca_zwykle;
    }

    public void setMiejsca_vip(Boolean[] miejsca_vip) {
        this.miejsca_vip = miejsca_vip;
    }

    public void setCena_zwykly(int cena_zwykly) {
        this.cena_zwykly = cena_zwykly;
    }

    public void setCena_vip(int cena_vip) {
        this.cena_vip = cena_vip;
    }



    public Seans(int id_se, int id_sali, int idf, LocalDate dzien, Time godzina, Boolean[] miejsca_zwykle, Boolean[] miejsca_vip, String tytul, String zdjecie, int cena_zwykly, int cena_vip) {
        this.id_se = id_se;
        this.id_sali = id_sali;
        this.idf = idf;
        this.dzien = dzien;
        this.godzina = godzina;
        this.miejsca_zwykle = miejsca_zwykle;
        this.miejsca_vip = miejsca_vip;
        this.tytul = tytul;
        this.zdjecie = zdjecie;
        this.cena_zwykly = cena_zwykly;
        this.cena_vip = cena_vip;
    }



    public Seans(){}

    @Override
    public String toString() {
        return "Seans{" +
                "id_se=" + id_se +
                ", id_sali=" + id_sali +
                ", idf=" + idf +
                ", dzien=" + dzien +
                ", godzina=" + godzina +
                ", miejsca_zwykle=" + Arrays.toString(miejsca_zwykle) +
                ", miejsca_vip=" + Arrays.toString(miejsca_vip) +
                ", tytul='" + tytul + '\'' +
                ", zdjecie='" + zdjecie + '\'' +
                ", cena_zwykly=" + cena_zwykly +
                ", cena_vip=" + cena_vip +
                '}';
    }



}
