package pwsztar.edu.pl.project_kino.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pwsztar.edu.pl.project_kino.dto.Film;
import pwsztar.edu.pl.project_kino.service.FilmService;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(path = "api/v1/")
public class FilmController {


    @Autowired
    private final FilmService filmService;

    @Autowired
    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @CrossOrigin
    @GetMapping(path = "film")
    public Film getFilm(@RequestParam("id") int id)
    {
        return filmService.getFilm(id);
    }

    @CrossOrigin
    @GetMapping(path = "films")
    public List<Film> getFilms() {
        return filmService.getFilms();
    }


    @PostMapping(path = "addFilm")
    public void addMovies(@RequestBody ArrayList<Film> filmsToAdd){
        filmService.addFilms(filmsToAdd);
    }

    @PostMapping(path = "removeFilm")
    public void removeFilms(@RequestBody ArrayList<Film> filmsToRemove){
            filmService.deleteFilms(filmsToRemove);
        }


}
