package pwsztar.edu.pl.project_kino.rest;


import com.lowagie.text.DocumentException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pwsztar.edu.pl.project_kino.dto.Newsletter;
import pwsztar.edu.pl.project_kino.service.NewsletterService;
import pwsztar.edu.pl.project_kino.service.ZamowieniaService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(path = "testy/")
public class NewsletterController {

    private final NewsletterService newsletterService;
    private final ZamowieniaService zamowieniaService;


    public NewsletterController(NewsletterService newsletterService, ZamowieniaService zamowieniaService) {
        this.newsletterService = newsletterService;
        this.zamowieniaService = zamowieniaService;
    }


    @CrossOrigin
    @GetMapping(path = "testget")
    public List<Newsletter> getNewsletters(){
        return newsletterService.getEmailsFromDatabase();
    }

    @CrossOrigin
    @GetMapping(path = "testsave")
    public void zapiszEmail()
    {
        newsletterService.saveNewsletter("beqs111@gmail.com",true);
    }

    @CrossOrigin
    @GetMapping(path = "savezam")
    public void saveZam()
    {
        zamowieniaService.saveZamowienie(1,"test","test","test");
    }


    @CrossOrigin
    @GetMapping("cancel_newesletter")
    public ResponseEntity saveRezerwacja(@RequestParam(name = "adres_email")
                                                     String adres_email,
                                         HttpServletRequest request,
                                         HttpServletResponse resp,
                                         Model model) throws IOException, DocumentException, InterruptedException {


        System.out.println(adres_email);
        newsletterService.unsubscribeFromNewsletter(adres_email);
        return new ResponseEntity<>("yyyy", HttpStatus.OK);

    }





    }
