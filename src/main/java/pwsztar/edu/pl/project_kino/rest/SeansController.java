package pwsztar.edu.pl.project_kino.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pwsztar.edu.pl.project_kino.Response.RezerwacjaResponse;
import pwsztar.edu.pl.project_kino.dto.Film;
import pwsztar.edu.pl.project_kino.dto.Seans;
import pwsztar.edu.pl.project_kino.service.SeansService;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(path = "api/v1/")
public class SeansController {

    private final SeansService seansService;

    @Autowired
    public SeansController(SeansService seansService) { this.seansService = seansService; }

    @CrossOrigin
    @GetMapping(path = "seanse")
    public List<Seans> getSeanses() {
        return seansService.getSeanses();
    }

    @CrossOrigin
    @GetMapping(path = "seans")
    public Seans getSeans(@RequestParam("id") int id)
    {
        return seansService.getSeans(id);
    }


    @PostMapping(path = "addSeans")
    public void addMovies(@RequestBody ArrayList<Seans> seansesToAdd){
        seansService.addSeanses(seansesToAdd);
    }

    @PostMapping(path = "removeSeans")
    public void removeFilms(@RequestBody ArrayList<Integer> seansesToRemove){
        seansService.deleteSeanses(seansesToRemove);
    }

}