package pwsztar.edu.pl.project_kino.rest;


import com.google.zxing.WriterException;
import com.lowagie.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import pwsztar.edu.pl.project_kino.service.QRCodeGenerator;
import pwsztar.edu.pl.project_kino.service.EmailSender;
import pwsztar.edu.pl.project_kino.service.ZamHelper;
import pwsztar.edu.pl.project_kino.Response.RezerwacjaResponse;
import pwsztar.edu.pl.project_kino.dto.Rezerwacja;
import pwsztar.edu.pl.project_kino.dto.Zam;
import pwsztar.edu.pl.project_kino.service.*;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


    @RestController
    @RequestMapping("/orders")
    public class PDFController {

        private final SeansService seansService;
        private final NewsletterService newsletterService;
        private final ZamowieniaService zamowieniaService;
        private Rezerwacja rezerwacja;
        @Autowired
        ServletContext servletContext;

        private final TemplateEngine templateEngine;
        private JavaMailSender javaMailSender;
        private final EmailSender emailSender;
        public PDFController(SeansService seansService, NewsletterService newsletterService, ZamowieniaService zamowieniaService, TemplateEngine templateEngine, EmailSender emailSender) {
            this.seansService = seansService;
            this.newsletterService = newsletterService;
            this.zamowieniaService = zamowieniaService;
            this.emailSender = emailSender;
            this.templateEngine = templateEngine;
        }


        @CrossOrigin
        @GetMapping("/rezerwacja")
        public ResponseEntity saveRezerwacja(@RequestParam(name = "imie") String imie,
                                             @RequestParam(name = "nazwisko") String nazwisko,
                                             @RequestParam(name = "nazwa_filmu") String nazwa_filmu,
                                             @RequestParam(name = "data") String data,
                                              @RequestParam(name = "godz") String godz,
                                              @RequestParam(name = "cena") Float cena,
                                              @RequestParam(name = "rzad") Integer rzad,
                                              @RequestParam(name = "miejsce") Integer miejsce,
                                              @RequestParam(name = "typ") String typ,
                                              @RequestParam(name = "id_seansu") int id_seansu,
                                              @RequestParam(name = "email") String email,
                                              @RequestParam(name = "zgoda") Boolean zgoda,
                                          HttpServletRequest request, HttpServletResponse resp, Model model) throws IOException, DocumentException, InterruptedException {
            rezerwacja=new Rezerwacja(imie,nazwisko,nazwa_filmu,data,godz,cena,rzad-1,miejsce-1,typ,email){};

            newsletterService.saveNewsletter(email,zgoda);
            zamowieniaService.saveZamowienie(id_seansu,imie,nazwisko,email);

            int seat= ((rezerwacja.getRzad())*9)+(rezerwacja.getMiejsce());
            RezerwacjaResponse response= seansService.checkIfAvaliavleV2(rezerwacja,id_seansu,seat);
            getQRCode();
            if(response.isValidated()==true)
            {
                seansService.saveSeat(id_seansu,seat,rezerwacja.getTyp());
//                getQRCode(model);
                getPDF(request,resp);
                return new ResponseEntity<>("ok", HttpStatus.OK);

            }else
                return new ResponseEntity<>("blad", HttpStatus.BAD_REQUEST);



        }


        @CrossOrigin
        @GetMapping("/qr")
        public void getQRCode(){
            String github= rezerwacja.getNazwa_filmu() +" "+ rezerwacja.getData()+" "+ rezerwacja.getGodz();

            byte[] image = new byte[0];
            try {
                QRCodeGenerator.generateQRCodeImage(github,250,250,"./src/main/resources/static/QRCode.png");

            } catch (WriterException | IOException e) {
                e.printStackTrace();
            }


        }
        public void getPDF(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException, FileNotFoundException {

            ZamHelper zamHelper = new ZamHelper(rezerwacja.getTyp(), rezerwacja.getNazwa_filmu(), rezerwacja.getData() + " " + rezerwacja.getGodz(), rezerwacja.getImie()+ " " +rezerwacja.getNazwisko(), rezerwacja.getCena(),rezerwacja.getMiejsce()+1,rezerwacja.getRzad()+1,"QRCode.png");
            Zam zam = zamHelper.getZam();
            WebContext context = new WebContext(request, response, servletContext);
            context.setVariable("zamEntry", zam);
            String zamHtml = templateEngine.process("zam", context);
            ByteArrayOutputStream target = new ByteArrayOutputStream();
            ConverterProperties converterProperties = new ConverterProperties();
            converterProperties.setBaseUri("http://localhost:8080");
            HtmlConverter.convertToPdf(zamHtml, target, converterProperties);

            byte[] bytes = target.toByteArray();
            OutputStream os = new FileOutputStream("pdf/thyme.pdf");
            os.write(bytes);
            os.close();
            String body = ("Rezerwacja na senas:"+" "+ rezerwacja.getNazwa_filmu() +" "+ rezerwacja.getData() + " godz. " + rezerwacja.getGodz());
            File f = new File("pdf/thyme.pdf");
            emailSender.sendEmail(rezerwacja.getEmail(), "Kino rezerwacja biletu", body,f);

        }


    }




//}