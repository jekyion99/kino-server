package pwsztar.edu.pl.project_kino.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pwsztar.edu.pl.project_kino.dto.Film;

@Repository
public interface FilmRepository extends JpaRepository<Film,Integer> {

}
