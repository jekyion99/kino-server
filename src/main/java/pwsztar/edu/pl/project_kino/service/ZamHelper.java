package pwsztar.edu.pl.project_kino.service;

import pwsztar.edu.pl.project_kino.dto.Cena;
import pwsztar.edu.pl.project_kino.dto.SeansPDF;
import pwsztar.edu.pl.project_kino.dto.Zam;

public class ZamHelper {

    private Zam zam;

    public ZamHelper(String typ, String nazwa, String data, String imie, float cena2, Integer rzad, Integer miejsce, String s){
        zam = new Zam();
        zam.setTyp(typ);
        SeansPDF seans = new SeansPDF();
        seans.setNazwaSeansu(nazwa);
        seans.setDataSeansu(data);
        seans.setImieNazwisko(imie);
        seans.setMiejsce(rzad);
        seans.setRzad(miejsce);
        seans.setQr(s);
        zam.setSeans(seans);
        Cena cena= new Cena();
        cena.setIlosc(cena2);
        zam.setCena(cena);
    }



    public Zam getZam() {
        return zam;
    }
}