package pwsztar.edu.pl.project_kino.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pwsztar.edu.pl.project_kino.dto.Film;
import pwsztar.edu.pl.project_kino.dto.Newsletter;

import java.util.List;

@Service
public class NewsletterService {

    private final NewsletterRepository newsletterRepository;
    private final MailSender mailSender;

    public NewsletterService(NewsletterRepository newsletterRepository) {
        this.newsletterRepository = newsletterRepository;
        this.mailSender = new MailSender();
    }

    public void unsubscribeFromNewsletter(String email){

        List<Newsletter> newsletterList = getEmailsFromDatabase();


        for(Newsletter tmp:newsletterList)
        {
            if(tmp.getEmail().equals(email))
            {
                System.out.println(tmp.getNews_id());
                Newsletter newsletter = newsletterRepository.getOne(tmp.getNews_id());
//                newsletter.setZgoda(false);
                newsletterRepository.delete(newsletterRepository.getOne(tmp.getNews_id()));
//                newsletterRepository.save(newsletter);
            }
        }

    }



    public void saveNewsletter(String email, Boolean zgoda)
    {

        List<Newsletter> newsletterList = getEmailsFromDatabase();
        Boolean flaga=false;

        for(Newsletter tmp:newsletterList)
        {
            if(tmp.getEmail().equals(email))flaga=true;
        }
        if(!flaga) {
            Newsletter newsletter = new Newsletter(email, zgoda);
            newsletterRepository.save(newsletter);
        }
    }

    public List<Newsletter> getEmailsFromDatabase()
    {
        return newsletterRepository.findAll();
    }

    public void sendEmails(List<Film> films) {

        String subject;
        String message;
        if(films.size()>1){
             subject = "Nowe filmy w naszymm kinie!";
             message = "Nasze kino dodało do repertuaru nowe filmy:\n";
        }
        else
        {
            subject = "Nowy film w naszymm kinie!";
            message = "Nasze kino dodało do repertuaru nowy film:\n";
        }

        for(Film film: films)
        {
            message+="- " + film.getTytul()+"\n";

        }

        message+="\nPrzyjdź i zobacz!";

        mailSender.sendMessages(getEmailsFromDatabase(), subject, message);
    }




}
