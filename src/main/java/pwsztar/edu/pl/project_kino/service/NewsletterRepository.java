package pwsztar.edu.pl.project_kino.service;


import lombok.Setter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pwsztar.edu.pl.project_kino.dto.Newsletter;

@Repository
public interface NewsletterRepository extends JpaRepository<Newsletter,Integer> {


}
