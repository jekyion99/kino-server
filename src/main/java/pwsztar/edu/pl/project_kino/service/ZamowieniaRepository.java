package pwsztar.edu.pl.project_kino.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pwsztar.edu.pl.project_kino.dto.Zamowienia;

@Repository
public interface ZamowieniaRepository extends JpaRepository<Zamowienia,Integer> {

}
